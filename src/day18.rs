use std::collections::VecDeque;

const INPUT: &'static str = include_str!("../inputs/day18.txt");

#[derive(Debug)]
struct Key {
    row: usize,
    col: usize,
    c: char,
}

#[derive(Debug)]
struct GridMeta {
    input: Vec<Vec<char>>,
    row: usize,
    col: usize,
    spr: usize,
    spc: usize,
    keys: Vec<Key>,
    gates: Vec<[usize; 2]>,
}

fn part1(input: &str) -> u32 {
    let mut grid = parse_input(input);
    let mut step_count = 0;

    let mut q: VecDeque<Key> = VecDeque::new();
    q.push_back(Key {
        row: grid.spr,
        col: grid.spc,
        c: '@',
    });

    while !q.is_empty() {}

    step_count
}

fn parse_input(rinput: &str) -> GridMeta {
    let mut row = 0;
    let mut col = 0;
    let mut spr = 0;
    let mut spc = 0;
    let mut keys = vec![];
    let mut gates = vec![];

    for line in rinput.lines() {
        col = 0;
        for c in line.chars() {
            match c {
                '@' => {
                    spr = row;
                    spc = col;
                }
                _ if c.is_lowercase() => keys.push(Key { row, col, c }),
                _ if c.is_uppercase() => gates.push([row, col]),
                '.' | '#' => (),
                _ => unreachable!(),
            }

            col += 1;
        }
        row += 1;
    }
    let input: Vec<Vec<char>> = rinput.lines().map(|x| x.chars().collect()).collect();

    GridMeta {
        input,
        row,
        col,
        spr,
        spc,
        keys,
        gates,
    }
}

fn main() {
    dbg!(part1(INPUT));
}

#[test]
fn day18_part1() {
    let map = [
        (
            r"#########
#b.A.@.a#
#########",
            8,
        ),
        (
            r"########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################",
            86,
        ),
        (
            r"########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################",
            132,
        ),
        (
            r"#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################",
            136,
        ),
        (
            r"########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################",
            81,
        ),
    ];

    for k in &map {
        assert_eq!(part1(k.0), k.1);
    }
}
