const INPUT: &[u8] = include_bytes!("../inputs/day12.txt");

#[derive(Debug)]
struct Moon {
    pos: [isize; 3],
    vel: [isize; 3],
}

fn parse_moon(input: &[u8]) -> Result<Moon, &'static str> {
    use nom::bytes::complete::tag;
    let (input, _) = tag("<x=")(input)?;
    let (input, x) = signed_number::<i32>(input)?;
    let (input, _) = tag(", y=")(input)?;
    let (input, y) = signed_number::<i32>(input)?;
    let (input, _) = tag(", z=")(input)?;
    let (input, z) = signed_number::<i32>(input)?;
    let (input, _) = tag(">")(input)?;
    Moon {
        pos: [x, y, z],
        vel: [0, 0, 0],
    }
}
fn part1(input: &str) -> usize {
    let input: Vec<Moon> = input.lines().map(parse).collect();

    0
}

fn main() {
    dbg!(part1(INPUT));
}
