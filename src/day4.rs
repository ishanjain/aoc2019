fn main() {
    dbg!(part1(372304, 847060));
    dbg!(part2(372304, 847060));
}

fn part1(start: u32, end: u32) -> u32 {
    let mut combos = 0;

    for i in start..=end {
        if validate_increasing_order(i) && validate_pair_exist(i) {
            combos += 1;
        }
    }

    combos
}

fn part2(start: u32, end: u32) -> u32 {
    let mut combos = 0;

    for i in start..=end {
        if validate_increasing_order(i) && validate_pair_exist_strict(i) {
            combos += 1;
        }
    }

    combos
}

#[inline]
fn validate_increasing_order(n: u32) -> bool {
    // Check increasing order
    let mut q = n;
    let mut largest = 0;
    let mut i = 1;
    while q != 0 {
        let d = q / (100_00_00 / 10u32.pow(i));

        if d >= largest {
            largest = d;
        } else {
            return false;
        }
        q %= 100_00_00 / 10u32.pow(i);

        i += 1;
    }
    // zeros in lower positions means it'll fail the increasing order of digits condition
    if n % 10 == 0 {
        return false;
    }

    true
}

#[inline]
fn validate_pair_exist(mut n: u32) -> bool {
    let mut digits = [0u32; 10];

    while n != 0 {
        let d = n % 10;
        digits[d as usize] += 1;
        n /= 10;
    }

    digits.into_iter().filter(|x| x >= &&2).count() != 0
}

#[inline]
fn validate_pair_exist_strict(mut n: u32) -> bool {
    let mut digits = [0u32; 10];

    while n != 0 {
        let d = n % 10;

        digits[d as usize] += 1;

        n /= 10;
    }

    digits.into_iter().filter(|x| x == &&2).count() != 0
}
