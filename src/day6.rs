const INPUT: &'static str = include_str!("../inputs/day6.txt");

use std::collections::{HashMap, HashSet};

fn part1(input: &str) -> usize {
    let input: HashMap<String, String> = input
        .lines()
        .map(|x| {
            let mut q = x.split(")");
            let (src, dst) = (q.next().unwrap(), q.next().unwrap());
            (dst.to_owned(), src.to_owned())
        })
        .collect();

    let direct_orbit_count = input.len();
    let mut indirect_orbit_count = 0;

    println!("{:?}", input);

    //    for (k, v) in input {
    //        let mut local_indirect_orbit = 0;
    //        let mut current_value = v;
    //
    //        while let Some(next) = input.get(current_value) {
    //            local_indirect_orbit += 1;
    //            if next == "COM" {
    //                break;
    //            }
    //            current_value = next;
    //        }
    //
    //        indirect_orbit_count += local_indirect_orbit;
    //    }

    direct_orbit_count + indirect_orbit_count
}

fn part2(input: &str) -> usize {
    0
}

fn main() {
    dbg!(part1(INPUT));
    dbg!(part2(INPUT));
}

#[test]
fn day6_part1() {
    let input = "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L";
    assert_eq!(part1(input), 42);
}

#[test]
fn day6_part2() {
    let input = "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN";

    assert_eq!(part2(input), 4);
}

fn part1_crappy(input: &str) -> usize {
    let mut map: HashMap<&str, HashSet<&str>> = HashMap::new();

    for line in input.lines() {
        let line: Vec<&str> = line.split(")").collect();
        let s = line[0];
        let d = line[1];

        let v = map.entry(s).or_insert(HashSet::new());
        v.insert(d);
    }
    loop {
        let mut map2 = map.clone();

        for (_k, v) in &mut map {
            for i in v.clone().iter() {
                let q = map2.entry(i).or_insert(HashSet::new());
                v.extend(q.iter());
            }
        }

        if sum(&map) == sum(&map2) {
            return sum(&map);
        }
    }
}

fn sum(map: &HashMap<&str, HashSet<&str>>) -> usize {
    let mut count = 0;

    for (_, v) in map {
        count += v.len();
    }

    count
}
