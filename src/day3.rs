const INPUT: &'static str = include_str!("../inputs/day3.txt");

#[derive(Debug)]
enum Move {
    Left(i64),
    Right(i64),
    Up(i64),
    Down(i64),
}

fn parse_input(input: &str) -> Vec<Vec<Move>> {
    let mut output = vec![];

    for input in input.split("\n") {
        let mut out = vec![];

        for input in input.split(",") {
            let weight = input.chars().skip(1).collect::<String>();
            let weight = weight.parse::<i64>().unwrap();

            match input.chars().nth(0) {
                Some('R') => out.push(Move::Right(weight)),
                Some('L') => out.push(Move::Left(weight)),
                Some('U') => out.push(Move::Up(weight)),
                Some('D') => out.push(Move::Down(weight)),
                _ => unreachable!(),
            }
        }
        output.push(out);
    }
    output
}

fn move_pos(dir: &Move, x: i64, y: i64) -> (i64, i64) {
    let mut xn = 0;
    let mut yn = 0;
    match dir {
        Move::Left(v) => xn = x - v,
        Move::Right(v) => xn = x + v,
        Move::Up(v) => yn = y + v,
        Move::Down(v) => yn = y - v,
    }
    (xn, yn)
}

fn lines_intersect(src: (i64, i64, i64, i64), dst: (i64, i64, i64, i64)) -> Option<(i64, i64)> {
    let (sx0, sy0, sx1, sy1) = src;
    let (dx0, dy0, dx1, dy1) = dst;

    None
}

fn part1(input: &str) -> i64 {
    let input = parse_input(input);
    let l1 = &input[0];
    let l2 = &input[1];
    let (mut x0, mut y0) = (0, 0);
    let (mut x1, mut y1) = (0, 0);

    let mut min_distance = std::i64::MAX;

    for m1 in l1 {
        let (dx0, dy0) = move_pos(m1, x0, y0);

        for m2 in l2 {
            let (dx1, dy1) = move_pos(m2, x1, y1);
            println!("{} {} {} {}", x0, y0, x1, y1);

            // Points intersect!
            // Calculate distance from origin/Starting point
            let distance = x1.abs() + y1.abs();

            if distance < min_distance {
                min_distance = distance;
            }
        }

        x1 = 0;
        y1 = 0;
    }
    min_distance
}

fn main() {
    dbg!(part1(INPUT));
}

#[test]
fn day3_part1() {
    let input = "R8,U5,L5,D3
U7,R6,D4,L4";
    assert_eq!(part1(input), 6);

    let input = "R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83";

    assert_eq!(part1(input), 159);

    let input = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";

    assert_eq!(part1(input), 135);
}
