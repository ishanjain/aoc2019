const INPUT: &'static str = include_str!("../inputs/day2.txt");

fn intcodes() -> Vec<usize> {
    INPUT.split(",").map(|x| x.parse().unwrap()).collect()
}

fn part1(mut seq: Vec<usize>, p1: usize, p2: usize) -> usize {
    let mut i = 0;
    seq[1] = p1;
    seq[2] = p2;

    while i < seq.len() {
        let code = seq[i];

        match code {
            1 => {
                let o1s = seq[i + 1];
                let o2s = seq[i + 2];
                let d = seq[i + 3];

                seq[d] = seq[o1s] + seq[o2s];

                i += 4;
            }
            2 => {
                let o1 = seq[i + 1];
                let o2 = seq[i + 2];
                let d = seq[i + 3];

                seq[d] = seq[o1] * seq[o2];

                i += 4;
            }
            99 => break,
            _ => unreachable!(),
        }
    }

    seq[0]
}

fn part2(seq: Vec<usize>) -> usize {
    let mnum = 19690720;

    for n in 0..=99 {
        for v in 0..=99 {
            let s = seq.clone();
            if part1(s, n, v) == mnum {
                return 100 * n + v;
            };
        }
    }
    0
}

fn main() {
    let seq = intcodes();
    dbg!(part1(seq.clone(), 12, 2));
    dbg!(part2(seq));
}

#[test]
fn day2_part1() {
    assert_eq!(part1(vec![1, 0, 0, 0, 99], 1, 0), 2);

    assert_eq!(part1(vec![2, 3, 0, 3, 99], 2, 3), 2);

    assert_eq!(part1(vec![2, 4, 4, 5, 99, 0], 2, 4), 2);

    assert_eq!(part1(vec![1, 1, 1, 4, 99, 5, 6, 0, 99], 1, 1), 30);
}
