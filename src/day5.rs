const INPUT: &'static str = include_str!("../inputs/day5.txt");

fn intcodes() -> Vec<usize> {
    INPUT.split(",").map(|x| x.parse().unwrap()).collect()
}

fn main() {
    let seq = intcodes();
    //    dbg!(part1(seq));
}

fn part1(mut seq: Vec<usize>, p1: usize, p2: usize) -> usize {
    let mut i = 0;
    seq[1] = p1;
    seq[2] = p2;

    while i < seq.len() {
        let code = seq[i];

        match code {
            1 => {
                let o1s = seq[i + 1];
                let o2s = seq[i + 2];
                let d = seq[i + 3];

                seq[d] = seq[o1s] + seq[o2s];

                i += 4;
            }
            2 => {
                let o1 = seq[i + 1];
                let o2 = seq[i + 2];
                let d = seq[i + 3];

                seq[d] = seq[o1] * seq[o2];

                i += 4;
            }
            3 => {}
            4 => {}
            99 => break,
            _ => unreachable!(),
        }
    }

    seq[0]
}
