const INPUT: &'static str = include_str!("../inputs/day1.txt");

fn masses() -> impl Iterator<Item = u64> {
    INPUT.lines().map(|x| x.parse::<u64>().unwrap())
}

fn calc_fuel(mass: u64) -> Option<u64> {
    let v = mass / 3;

    if v <= 2 {
        None
    } else {
        Some(v - 2)
    }
}

fn fuels(mut mass: u64) -> impl Iterator<Item = u64> {
    core::iter::from_fn(move || {
        let fuel = calc_fuel(mass)?;
        mass = fuel;
        Some(fuel)
    })
}

fn solution_part1() -> u64 {
    masses().flat_map(|x| fuels(x).next()).sum()
}

fn solution_part2() -> u64 {
    masses().flat_map(fuels).sum()
}

fn main() {
    dbg!(solution_part1());
    dbg!(solution_part2());
}
